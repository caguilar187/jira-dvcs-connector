package com.atlassian.jira.plugins.dvcs.dao;

import com.atlassian.jira.plugins.dvcs.activeobjects.v3.GitlabEventMapping;
import com.atlassian.jira.plugins.dvcs.model.Repository;

import java.util.Map;

/**
 * DAO layer of the {@link com.atlassian.jira.plugins.dvcs.activeobjects.v3.GitHubEventMapping}.
 *
 * @author Stanislav Dvorscak
 *
 */
public interface GitlabEventDAO
{

    /**
     * @param gitlabEvent
     *            values for creation - initial values of {@link com.atlassian.jira.plugins.dvcs.activeobjects.v3.GitHubEventMapping} entity
     * @return creates new GitHub even entry
     */
    GitlabEventMapping create(Map<String, Object> gitlabEvent);

    /**
     * Marks provided {@link com.atlassian.jira.plugins.dvcs.activeobjects.v3.GitHubEventMapping} as a save point - {@link com.atlassian.jira.plugins.dvcs.activeobjects.v3.GitHubEventMapping#isSavePoint()} == true.
     *
     * @param gitlabEvent
     *            for marking
     */
    void markAsSavePoint(GitlabEventMapping gitlabEvent);

    /**
     * Removes all events for provided repository.
     *
     * @param repository
     *            for which repository
     */
    void removeAll(Repository repository);

    /**
     * @param repository
     * @param gitHubId
     *            {@link GitlabEvent#getId()}
     * @return resolved {@link com.atlassian.jira.plugins.dvcs.activeobjects.v3.GitHubEventMapping} by remote id
     */
    GitlabEventMapping getByGitlabId(Repository repository, String gitlabId);

    /**
     * @param repository
     *            over which repository
     * @return Returns last {@link com.atlassian.jira.plugins.dvcs.activeobjects.v3.GitHubEventMapping}, which was marked as a save point - {@link com.atlassian.jira.plugins.dvcs.activeobjects.v3.GitHubEventMapping#isSavePoint()} == true.
     */
    GitlabEventMapping getLastSavePoint(Repository repository);

}
