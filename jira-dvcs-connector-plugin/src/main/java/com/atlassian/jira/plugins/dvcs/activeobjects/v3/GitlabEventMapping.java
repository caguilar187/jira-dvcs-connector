package com.atlassian.jira.plugins.dvcs.activeobjects.v3;

import net.java.ao.Entity;
import net.java.ao.schema.Default;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.Table;

import java.util.Date;

/**
 * AO representation of the {@link GitLabEvent}.
 * 
 * @author Stanislav Dvorscak
 * 
 */
@Table("GIT_LAB_EVENT")
public interface GitlabEventMapping extends Entity
{

    /**
     * @see #getRepository()
     */
    public static String REPOSITORY = "REPOSITORY_ID";

    /**
     * @see #getGitLabId()
     */
    public static String GIT_LAB_ID = "GIT_LAB_ID";

    /**
     * @see #getCreatedAt()
     */
    public static String CREATED_AT = "CREATED_AT";

    /**
     * @see #isSavePoint()
     */
    public static String SAVE_POINT = "SAVE_POINT";

    /**
     * @return {@link GitLabEvent#getRepository()}
     */
    @NotNull
    RepositoryMapping getRepository();

    /**
     * @return remote id of GithubEvent
     */
    @NotNull
    @Default ("0") // this has no effect other than to work around FUSE-705/AO-490
    String getGitLabId();

    /**
     * @param gitHubId
     *            {@link #getGitLabId()}
     */
    void setGitlabId(String gitHubId);

    /**
     * @return date of event creation
     */
    @NotNull
    Date getCreatedAt();

    /**
     * @param createdAt
     *            {@link #getCreatedAt()}
     */
    void setCreatedAt(Date createdAt);

    /**
     * @return true if this event can be considered to be a save point - it means all previous records was already proceed
     */
    boolean isSavePoint();

    /**
     * @param savePoint
     *            {@link #isSavePoint()}
     */
    void setSavePoint(boolean savePoint);

}
