package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.jira.plugins.dvcs.model.Progress;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.message.MessagingService;
import com.atlassian.jira.plugins.dvcs.spi.gitlab.service.GitlabEventContext;
import com.atlassian.jira.plugins.dvcs.spi.gitlab.message.GitlabPullRequestSynchronizeMessage;
import com.atlassian.jira.plugins.dvcs.sync.GitlabPullRequestSynchronizeMessageConsumer;
import com.atlassian.jira.plugins.dvcs.sync.Synchronizer;
import org.gitlab.api.models.GitlabMergeRequest;

import java.util.HashSet;
import java.util.Set;

/**
 * Context for GitHub event synchronisation
 *
 */
public class GitlabEventContextImpl implements GitlabEventContext
{
    private final Synchronizer synchronizer;
    private final MessagingService messagingService;

    private final Repository repository;
    private final boolean isSoftSync;
    private final String[] synchronizationTags;
    private final boolean webHookSync;

    private final Set<Long> processedPullRequests = new HashSet<Long>();

    public GitlabEventContextImpl(final Synchronizer synchronizer, final MessagingService messagingService, final Repository repository, final boolean softSync, final String[] synchronizationTags, boolean webHookSync)
    {
        this.synchronizer = synchronizer;
        this.messagingService = messagingService;
        this.repository = repository;
        isSoftSync = softSync;
        this.synchronizationTags = synchronizationTags;
        this.webHookSync = webHookSync;
    }

    @Override
    public void savePullRequest(GitlabMergeRequest pullRequest)
    {
        if (pullRequest == null || processedPullRequests.contains((long)pullRequest.getId()))
        {
            return;
        }

        processedPullRequests.add((long)pullRequest.getId());

        Progress progress = synchronizer.getProgress(repository.getId());
        GitlabPullRequestSynchronizeMessage message = new GitlabPullRequestSynchronizeMessage(progress, progress.getAuditLogId(),
                isSoftSync, repository, pullRequest.getIid(), webHookSync);

        messagingService.publish(
                messagingService.get(GitlabPullRequestSynchronizeMessage.class, GitlabPullRequestSynchronizeMessageConsumer.ADDRESS),
                message, synchronizationTags);
    }
}
