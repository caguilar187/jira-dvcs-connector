package com.atlassian.jira.plugins.dvcs.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.activeobjects.QueryHelper;
import com.atlassian.jira.plugins.dvcs.activeobjects.QueryHelper.OrderClause;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.GitlabEventMapping;
import com.atlassian.jira.plugins.dvcs.dao.GitlabEventDAO;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.util.ActiveObjectsUtils;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.transaction.TransactionCallback;
import net.java.ao.Query;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * AO implementation of the {@link com.atlassian.jira.plugins.dvcs.dao.GitHubEventDAO}.
 *
 * @author Stanislav Dvorscak
 *
 */
@Component
public class GitlabEventDAOImpl implements GitlabEventDAO
{

    /**
     * Injected {@link com.atlassian.activeobjects.external.ActiveObjects} dependency.
     */
    @Resource
    @ComponentImport
    private ActiveObjects activeObjects;

    /**
     * Injected {@link com.atlassian.jira.plugins.dvcs.activeobjects.QueryHelper} dependency.
     */
    @Resource
    private QueryHelper queryHelper;

    /**
     * {@inheritDoc}
     */
    @Override
    public GitlabEventMapping create(final Map<String, Object> gitlabEvent)
    {
        activeObjects.executeInTransaction(new TransactionCallback<Void>()
        {

            @Override
            public Void doInTransaction()
            {
                activeObjects.create(GitlabEventMapping.class, gitlabEvent);
                return null;
            }

        });
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void markAsSavePoint(final GitlabEventMapping gitlabEvent)
    {
        activeObjects.executeInTransaction(new TransactionCallback<Void>()
        {

            @Override
            public Void doInTransaction()
            {
                gitlabEvent.setSavePoint(true);
                gitlabEvent.save();
                return null;
            }

        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAll(Repository repository)
    {
        Query allForRepositoryQuery = Query.select().where(GitlabEventMapping.REPOSITORY + " = ? ", repository.getId());
        ActiveObjectsUtils.delete(activeObjects, GitlabEventMapping.class, allForRepositoryQuery);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GitlabEventMapping getByGitlabId(Repository repository, String gitlabId)
    {
        Query query = Query.select().where(GitlabEventMapping.REPOSITORY + " = ? AND " + GitlabEventMapping.GIT_LAB_ID + " = ? ", repository.getId(), gitlabId);
        GitlabEventMapping[] founded = activeObjects.find(GitlabEventMapping.class, query);
        if (founded.length > 1)
        {
            throw new RuntimeException("Multiple GitHubEvents exists with the same id: " + gitlabId);
        }
        return founded.length == 1 ? founded[0] : null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GitlabEventMapping getLastSavePoint(Repository repository)
    {
        Query query = Query.select();
        query.where(GitlabEventMapping.REPOSITORY + " = ? AND " + GitlabEventMapping.SAVE_POINT + " = ? ", repository.getId(), true);
        query.setOrderClause(queryHelper.getOrder(new OrderClause[] {
                new OrderClause(GitlabEventMapping.CREATED_AT, OrderClause.Order.ASC), new OrderClause("ID", OrderClause.Order.DESC) }));
        query.setLimit(1);

        GitlabEventMapping[] founded = activeObjects.find(GitlabEventMapping.class, query);
        return founded.length == 1 ? founded[0] : null;
    }

}
