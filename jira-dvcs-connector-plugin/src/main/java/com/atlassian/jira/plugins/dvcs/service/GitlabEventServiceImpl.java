package com.atlassian.jira.plugins.dvcs.service;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.activeobjects.v3.GitlabEventMapping;
import com.atlassian.jira.plugins.dvcs.dao.GitlabEventDAO;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.message.MessagingService;
import com.atlassian.jira.plugins.dvcs.spi.gitlab.GitlabClientProvider;
import com.atlassian.jira.plugins.dvcs.spi.github.service.GitHubEventProcessorAggregator;
import com.atlassian.jira.plugins.dvcs.spi.gitlab.service.GitlabEventService;
import com.atlassian.jira.plugins.dvcs.sync.Synchronizer;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.google.common.collect.Iterables;
import org.eclipse.egit.github.core.event.Event;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabMergeRequest;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the {@link com.atlassian.jira.plugins.dvcs.spi.github.service.GitHubEventService}.
 *
 * @author Stanislav Dvorscak
 *
 */
@Component
public class GitlabEventServiceImpl implements GitlabEventService
{

    /**
     * Injected {@link com.atlassian.jira.plugins.dvcs.dao.GitlabEventDAO} dependency.
     */
    @Resource
    private GitlabEventDAO gitlabEventDAO;

    /**
     * Injected {@link com.atlassian.activeobjects.external.ActiveObjects} dependency.
     */
    @Resource
    @ComponentImport
    private ActiveObjects activeObjects;

    /**
     * Injected {@link com.atlassian.jira.plugins.dvcs.spi.github.GithubClientProvider} dependency.
     */
    @Resource(name = "gitlabClientProvider")
    private GitlabClientProvider gitlabClientProvider;

    @Resource
    private Synchronizer synchronizer;

    @Resource
    MessagingService messagingService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAll(Repository repository)
    {
        gitlabEventDAO.removeAll(repository);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void synchronize(final Repository repository, final boolean isSoftSync, final String[] synchronizationTags, boolean webHookSync)
    {
        GitlabAPI gitlabAPI = gitlabClientProvider.createClient(repository);

        // gets repository ID
        String slug = repository.getSlug();

        final GitlabEventMapping lastGitHubEventSavePoint = gitlabEventDAO.getLastSavePoint(repository);

        Integer latestEventGitlabId = null;
        final GitlabEventContextImpl context = new GitlabEventContextImpl(synchronizer, messagingService, repository, isSoftSync, synchronizationTags, webHookSync);
        try
        {
            List<GitlabMergeRequest> requests = gitlabAPI.getAllMergeRequests(slug);
            for (final GitlabMergeRequest event : Iterables.concat(requests))
            {
                // processes single event - and returns flag if the processing of next records should be stopped, because their was already
                // proceed
                boolean shouldStop = activeObjects.executeInTransaction(new TransactionCallback<Boolean>()
                {

                    @Override
                    public Boolean doInTransaction()
                    {
                        // before, not before or equals - there can exists several events with the same timestamp, but it does not mean that
                        // all of them was already proceed
                        if (lastGitHubEventSavePoint != null && event.getCreatedAt().before(lastGitHubEventSavePoint.getCreatedAt()))
                        {
                            // all previous records was already proceed - we can stop events' iterating
                            return Boolean.TRUE;

                        }
                        else if (gitlabEventDAO.getByGitlabId(repository, event.getId().toString()) != null)
                        {
                            // maybe partial synchronization, and there can exist remaining events which was fired at the same time
                            // or save point was not marked and there can still exists entries which was not already proceed
                            return Boolean.FALSE;

                        }

                        // called registered GitHub event processors
                        context.savePullRequest(event);
                        saveEventCounterpart(repository, event);

                        return Boolean.FALSE;
                    }
                });

                if (shouldStop)
                {
                    break;
                }
                else if (latestEventGitlabId == null)
                {
                    latestEventGitlabId = event.getId();
                }
            }

        }
        catch (IOException e)
        {

        }
        // marks last event as a save point - because all previous records was fully proceed
        if (latestEventGitlabId != null)
        {
            gitlabEventDAO.markAsSavePoint(gitlabEventDAO.getByGitlabId(repository, latestEventGitlabId.toString()));
        }
    }

    /**
     * Stores provided {@link org.eclipse.egit.github.core.event.Event} locally as {@link com.atlassian.jira.plugins.dvcs.activeobjects.v3.GitHubEventMapping}. It is determined as marker that provided event was already
     * proceed.
     * 
     * @param repository
     *            over of event
     * @param event
     *            GitHub event which was proceed
     */
    private void saveEventCounterpart(Repository repository, GitlabMergeRequest event)
    {
        Map<String, Object> gitHubEvent = new HashMap<String, Object>();
        gitHubEvent.put(GitlabEventMapping.GIT_LAB_ID, event.getId());
        gitHubEvent.put(GitlabEventMapping.CREATED_AT, event.getCreatedAt());
        gitHubEvent.put(GitlabEventMapping.REPOSITORY, repository.getId());
        gitlabEventDAO.create(gitHubEvent);
    }

}
