package com.atlassian.jira.plugins.dvcs.spi.gitlab;

import java.io.IOException;
import java.net.URL;

import com.atlassian.jira.plugins.dvcs.auth.Authentication;
import com.atlassian.jira.plugins.dvcs.auth.AuthenticationFactory;
import com.atlassian.jira.plugins.dvcs.auth.impl.OAuthAuthentication;
import com.atlassian.jira.plugins.dvcs.exception.SourceControlException;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.util.DvcsConstants;
import com.atlassian.plugin.PluginAccessor;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.gitlab.api.GitlabAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("gitlabClientProvider")
public class GitlabClientProvider
{
    private final AuthenticationFactory authenticationFactory;

    @Autowired
    public GitlabClientProvider(AuthenticationFactory authenticationFactory, @ComponentImport PluginAccessor pluginAccessor)
    {
        this.authenticationFactory = authenticationFactory;
    }

    public GitlabAPI createClient(Repository repository)
    {
        OAuthAuthentication auth = (OAuthAuthentication) authenticationFactory.getAuthentication(repository);
        GitlabAPI client = createClientInternal(repository.getOrgHostUrl(), auth.getAccessToken());

        return client;
    }

    protected GitlabAPI createClientInternal(String url, String apiToken)
    {
        return createClient(url, apiToken);
    }

    public GitlabAPI createClient(Organization organization)
    {
        GitlabAPI client;
        Authentication authentication = authenticationFactory.getAuthentication(organization);
        if (authentication instanceof OAuthAuthentication)
        {
            OAuthAuthentication oAuth = (OAuthAuthentication) authentication;
            client = createClientInternal(organization.getHostUrl(), oAuth.getAccessToken());
        } else
        {
            throw new SourceControlException("Failed to get proper OAuth instance for Gitlab client.");
        }
        return client;
    }

    /**
     * Create a GitlabAPI to connect to the api.
     *
     * It uses the right host in case we're calling the Gitlab.com api.
     * It uses the right protocol in case we're calling the Gitlab Enterprise api.
     *
     * @param url is the Gitlab's oauth host.
     * @return a GitlabAPI
     */
    public static GitlabAPI createClient(String url, String apiToken)
    {
        GitlabAPI result = GitlabAPI.connect(url, apiToken);
        return result;
    }
}
