package com.atlassian.jira.plugins.dvcs.spi.gitlab.message;

import com.atlassian.jira.plugins.dvcs.service.message.AbstractMessagePayloadSerializer;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * {@link GitlabPullRequestPageMessage} message serializer.
 *
 * @author Miroslav Stencel <mstencel@atlassian.com>
 */
@Component
public class GitlabPullRequestPageMessageSerializer
        extends AbstractMessagePayloadSerializer<GitlabPullRequestPageMessage>
{
    @Override
    protected void serializeInternal(final JSONObject json, final GitlabPullRequestPageMessage payload) throws Exception
    {
        json.put("page", payload.getPage());
        json.put("pagelen", payload.getPagelen());
        json.put("processedPullRequests", payload.getProcessedPullRequests());
    }

    @Override
    protected GitlabPullRequestPageMessage deserializeInternal(final JSONObject json, final int version)
            throws Exception
    {
        Set<Long> processedPullRequests = asSet(json.optJSONArray("processedPullRequests"));
        return new GitlabPullRequestPageMessage(null, 0, false, null, json.getInt("page"), json.getInt("pagelen"), processedPullRequests, false);
    }

    @Override
    public Class<GitlabPullRequestPageMessage> getPayloadType()
    {
        return GitlabPullRequestPageMessage.class;
    }

    protected Set<Long> asSet(JSONArray optJSONArray)
    {
        if (optJSONArray == null)
        {
            return null;
        }

        Set<Long> ret = new HashSet<Long>();

        for (int i = 0; i < optJSONArray.length(); i++)
        {

            ret.add(optJSONArray.optLong(i));
        }
        return ret;
    }
}
