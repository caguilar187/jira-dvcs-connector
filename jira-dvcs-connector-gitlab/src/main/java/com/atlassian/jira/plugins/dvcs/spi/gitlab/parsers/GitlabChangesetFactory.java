package com.atlassian.jira.plugins.dvcs.spi.gitlab.parsers;

import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFile;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFileAction;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFileDetail;
import com.atlassian.jira.plugins.dvcs.util.CustomStringUtils;
import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang.StringUtils;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.gitlab.api.models.GitlabCommit;
import org.gitlab.api.models.GitlabCommitDiff;

/**
 * Factory for {@link Changeset} implementations
 */
public class GitlabChangesetFactory
{

    private GitlabChangesetFactory()
    {
    }

    public static Changeset transform(GitlabCommit gitlabCommit, List<GitlabCommitDiff> gitlabCommitDiffs, int repositoryId, String branch)
    {
        final List<ChangesetFileDetail> changesetFiles = transformToFileDetails(gitlabCommitDiffs);

        Date date = Calendar.getInstance().getTime();

        String email = gitlabCommit.getAuthorEmail();
        if (email == null)
            email = "";

        List<String> parentIds = gitlabCommit.getParentIds();
        if (parentIds == null)
            parentIds = Collections.<String>emptyList();

        String title = gitlabCommit.getTitle();
        String description = gitlabCommit.getDescription();

        Changeset changeset = new Changeset(
                repositoryId,
                gitlabCommit.getId(),
                gitlabCommit.getAuthorName(),
                email,
                date,
                "", // todo: raw-node. what is it in github?
                branch,
                title == null ? description
                    : description == null ? title
                    : title + description,
                parentIds,
                ImmutableList.<ChangesetFile>copyOf(changesetFiles),
                changesetFiles.size(),
                email
        );

        return changeset;
    }

    @SuppressWarnings("unchecked")
    public static List<ChangesetFileDetail> transformToFileDetails(List<GitlabCommitDiff> diffs)
    {
        if (diffs == null)
        {
            return Collections.<ChangesetFileDetail>emptyList();
        }

        return (List<ChangesetFileDetail>) CollectionUtils.collect(diffs, new Transformer() {

            @Override
            public Object transform(Object input)
            {
                GitlabCommitDiff commitDiff = (GitlabCommitDiff) input;

                String filename = commitDiff.getNewPath();
                int additions = 0;//commitDiff.getAdditions();
                int deletions = 0;//commitDiff.getDeletions();

                return new ChangesetFileDetail(getChangesetFileAction(commitDiff),filename,additions,deletions);
            }
        });
    }

    private static ChangesetFileAction getChangesetFileAction(GitlabCommitDiff diff)
    {
        if (diff.getNewFile())
        {
            return ChangesetFileAction.ADDED;
        }
        else if (diff.getDeletedFile())
        {
            return ChangesetFileAction.REMOVED;
        }
        else
        {
            return ChangesetFileAction.MODIFIED;
        }
    }

    /*
    public static Changeset transform(RepositoryCommit repositoryCommit, int repositoryId, String branch)
    {
        final List<ChangesetFile> changesetFiles = transformFiles(repositoryCommit.getFiles());

        String name = "";
        String authorEmail = null;

        Date date = Calendar.getInstance().getTime();

        if (repositoryCommit.getCommit() != null
                && repositoryCommit.getCommit().getAuthor() != null)
        {

            if (StringUtils.isNotBlank(repositoryCommit.getCommit().getAuthor().getName()))
            {
                name = repositoryCommit.getCommit().getAuthor().getName();
            }
            // TODO commit date is in getCommit().getCommitter().getDate(), should we use this instead
            // after git commit --amend command, the dates were different
            date = repositoryCommit.getCommit().getAuthor().getDate();
            authorEmail = repositoryCommit.getCommit().getAuthor().getEmail();
        }

        // try to get login from Author, if there is no Author try from Commiter
        String login = getUserLogin(repositoryCommit.getAuthor());
        if (StringUtils.isBlank(login))
        {
            login = getUserLogin(repositoryCommit.getCommitter());
        }

        Changeset changeset = new Changeset(
                repositoryId,
                repositoryCommit.getSha(),
                name,
                login,
                date,
                "", // todo: raw-node. what is it in github?
                branch,
                repositoryCommit.getCommit().getMessage(),
                transformParents(repositoryCommit.getParents()),
                changesetFiles,
                changesetFiles.size(),
                authorEmail
        );

		return changeset;
    }

    private static String getUserLogin(User user)
    {
        if (user!=null && user.getLogin()!=null)
        {
            return user.getLogin();
        }
        return "";
    }

    @SuppressWarnings("unchecked")
    private static List<ChangesetFile> transformFiles(List<CommitFile> files)
    {
        if (files == null)
        {
            return Collections.<ChangesetFile>emptyList();
        }

        return (List<ChangesetFile>) CollectionUtils.collect(files, new Transformer() {

            @Override
            public Object transform(Object input)
            {
                CommitFile commitFile = (CommitFile) input;

                String filename = commitFile.getFilename();
                String status = commitFile.getStatus();
                int additions = commitFile.getAdditions();
                int deletions = commitFile.getDeletions();

                return new ChangesetFile(CustomStringUtils.getChangesetFileAction(status),
                        filename, additions, deletions);
            }
        });
    }

    @SuppressWarnings("unchecked")
    private static List<String> transformParents(List<Commit> parents)
    {
        if (parents == null)
        {
            return Collections.<String>emptyList();
        }

        return (List<String>) CollectionUtils.collect(parents, new Transformer() {

            @Override
            public Object transform(Object input)
            {
                Commit commit = (Commit) input;

                return commit.getSha();
            }
        });
    }
    */
}
