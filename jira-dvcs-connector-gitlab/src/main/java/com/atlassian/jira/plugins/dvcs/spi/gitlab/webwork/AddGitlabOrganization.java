package com.atlassian.jira.plugins.dvcs.spi.gitlab.webwork;

import static com.atlassian.jira.plugins.dvcs.analytics.DvcsConfigAddEndedAnalyticsEvent.FAILED_REASON_OAUTH_GENERIC;
import static com.atlassian.jira.plugins.dvcs.analytics.DvcsConfigAddEndedAnalyticsEvent.FAILED_REASON_OAUTH_SOURCECONTROL;
import static com.atlassian.jira.plugins.dvcs.analytics.DvcsConfigAddEndedAnalyticsEvent.FAILED_REASON_VALIDATION;
import static com.atlassian.jira.plugins.dvcs.spi.gitlab.GitlabCommunicator.GITLAB;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.plugins.dvcs.auth.OAuthStore;
import com.atlassian.jira.plugins.dvcs.auth.OAuthStore.Host;
import com.atlassian.jira.plugins.dvcs.exception.SourceControlException;
import com.atlassian.jira.plugins.dvcs.model.AccountInfo;
import com.atlassian.jira.plugins.dvcs.model.Credential;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.service.OrganizationService;
import com.atlassian.jira.plugins.dvcs.spi.gitlab.GitlabCommunicator;
import com.atlassian.jira.plugins.dvcs.util.CustomStringUtils;
import com.atlassian.jira.plugins.dvcs.util.SystemUtils;
import com.atlassian.jira.plugins.dvcs.webwork.CommonDvcsConfigurationAction;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.sal.api.ApplicationProperties;

@Scanned
public class AddGitlabOrganization extends CommonDvcsConfigurationAction
{
    private final Logger log = LoggerFactory.getLogger(AddGitlabOrganization.class);

    public static final String EVENT_TYPE_GITLAB = "gitlab";

    private String url;
    private String organization;

    private String urlGitlab;
    private String apiKeyGitlab;

    // sent by GH on the way back
    private String code;

    private final OrganizationService organizationService;
    private final OAuthStore oAuthStore;
    private final ApplicationProperties applicationProperties;

    public AddGitlabOrganization(@ComponentImport ApplicationProperties applicationProperties,
                                 @ComponentImport EventPublisher eventPublisher,
                                 OAuthStore oAuthStore,
                                 OrganizationService organizationService)
    {
        super(eventPublisher);
        this.organizationService = organizationService;
        this.oAuthStore = oAuthStore;
        this.applicationProperties = applicationProperties;
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception
    {
        triggerAddStartedEvent(EVENT_TYPE_GITLAB);

        oAuthStore.store(new Host(GITLAB, urlGitlab), null, apiKeyGitlab);

        return doFinish();
    }

    @Override
    protected void doValidation()
    {
        if (StringUtils.isBlank(urlGitlab) || StringUtils.isBlank(apiKeyGitlab))
        {
            addErrorMessage("Please provide both url and api key parameters.");
        }

        if (organizationService.getByHostAndName(url, organization) != null)
        {
            addErrorMessage("Account is already integrated with JIRA.");
        }

        if (invalidInput())
        {
            triggerAddFailedEvent(FAILED_REASON_VALIDATION);
        }
    }

    public String doFinish()
    {
        try
        {
            return doAddOrganization(apiKeyGitlab);
        } catch (SourceControlException sce)
        {
            addErrorMessage(sce.getMessage());
            log.warn(sce.getMessage());
            if ( sce.getCause() != null )
            {
                log.warn("Caused by: " + sce.getCause().getMessage());
            }
            triggerAddFailedEvent(FAILED_REASON_OAUTH_SOURCECONTROL);
            return INPUT;

        } catch (Exception e) {
            addErrorMessage("Error obtain access token.");
            triggerAddFailedEvent(FAILED_REASON_OAUTH_GENERIC);
            return INPUT;
        }
    }

    private String doAddOrganization(String apiKey)
    {
        try
        {
            Organization newOrganization = new Organization();
            newOrganization.setName(organization);
            newOrganization.setHostUrl(urlGitlab);
            newOrganization.setDvcsType(GitlabCommunicator.GITLAB);
            newOrganization.setAutolinkNewRepos(hadAutolinkingChecked());
            newOrganization.setCredential(new Credential(null,
                    null, apiKey));
            newOrganization.setSmartcommitsOnNewRepos(hadAutolinkingChecked());

            organizationService.save(newOrganization);

        } catch (SourceControlException e)
        {
            addErrorMessage("Failed adding the account: [" + e.getMessage() + "]");
            log.debug("Failed adding the account: [" + e.getMessage() + "]");
            triggerAddFailedEvent(FAILED_REASON_OAUTH_SOURCECONTROL);
            return INPUT;
        }

        triggerAddSucceededEvent(EVENT_TYPE_GITLAB);
        return getRedirect("ConfigureDvcsOrganizations.jspa?atl_token=" + CustomStringUtils.encode(getXsrfToken()) +
                            getSourceAsUrlParam());
    }

    public static String encode(String url)
    {
        return CustomStringUtils.encode(url);
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getUrlGitlab()
    {
        return urlGitlab;
    }

    public void setUrlGitlab(String urlGitlab)
    {
        this.urlGitlab = urlGitlab;
    }

    public String getApiKeyGitlab()
    {
        return apiKeyGitlab;
    }

    public void setApiKeyGitlab(String apiKeyGitlab)
    {
        this.apiKeyGitlab = apiKeyGitlab;
    }

    public String getOrganization()
    {
        return organization;
    }

    public void setOrganization(String organization)
    {
        this.organization = organization;
    }

    private void triggerAddFailedEvent(String reason)
    {
        super.triggerAddFailedEvent(EVENT_TYPE_GITLAB, reason);
    }
}
