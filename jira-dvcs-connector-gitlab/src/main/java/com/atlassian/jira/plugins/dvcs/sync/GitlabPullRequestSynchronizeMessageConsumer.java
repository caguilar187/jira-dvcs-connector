package com.atlassian.jira.plugins.dvcs.sync;

import com.atlassian.jira.plugins.dvcs.activity.RepositoryCommitMapping;
import com.atlassian.jira.plugins.dvcs.activity.RepositoryPullRequestDao;
import com.atlassian.jira.plugins.dvcs.activity.RepositoryPullRequestMapping;
import com.atlassian.jira.plugins.dvcs.model.Message;
import com.atlassian.jira.plugins.dvcs.model.Participant;
import com.atlassian.jira.plugins.dvcs.model.PullRequestStatus;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.message.MessageAddress;
import com.atlassian.jira.plugins.dvcs.service.message.MessageConsumer;
import com.atlassian.jira.plugins.dvcs.service.message.MessagingService;
import com.atlassian.jira.plugins.dvcs.spi.gitlab.GitlabClientProvider;
import com.atlassian.jira.plugins.dvcs.spi.gitlab.message.GitlabPullRequestSynchronizeMessage;
import com.atlassian.jira.plugins.dvcs.util.ActiveObjectsUtils;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabCommit;
import org.gitlab.api.models.GitlabMergeRequest;
import org.gitlab.api.models.GitlabNote;
import org.gitlab.api.models.GitlabProject;
import org.gitlab.api.models.GitlabUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Message consumer {@link GitlabPullRequestSynchronizeMessage}.
 * 
 * @author Stanislav Dvorscak
 * 
 */
@Component
public class GitlabPullRequestSynchronizeMessageConsumer implements MessageConsumer<GitlabPullRequestSynchronizeMessage>
{

    /**
     * Logger of this class.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(GitlabPullRequestSynchronizeMessageConsumer.class);

    /**
     * @see #getQueue()
     */
    public static final String QUEUE = GitlabPullRequestSynchronizeMessageConsumer.class.getCanonicalName();

    /**
     * @see #getAddress()
     */
    public static final String ADDRESS = GitlabPullRequestSynchronizeMessage.class.getCanonicalName();

    /**
     * Injected {@link com.atlassian.jira.plugins.dvcs.activity.RepositoryPullRequestDao} dependency.
     */
    @Resource
    private RepositoryPullRequestDao repositoryPullRequestDao;

    /**
     * Injected {@link com.atlassian.jira.plugins.dvcs.service.PullRequestService} dependency.
     */
    @Resource
    private com.atlassian.jira.plugins.dvcs.service.PullRequestService pullRequestService;

    /**
     * Injected {@link GitlabClientProvider} dependency.
     */
    @Resource(name = "gitlabClientProvider")
    private GitlabClientProvider gitlabClientProvider;

    /**
     * Injected {@link com.atlassian.jira.plugins.dvcs.service.message.MessagingService} dependency.
     */
    @Resource
    private MessagingService messagingService;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getQueue()
    {
        return QUEUE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onReceive(Message<GitlabPullRequestSynchronizeMessage> message, GitlabPullRequestSynchronizeMessage payload)
    {
        Repository repository = payload.getRepository();

        GitlabMergeRequest remoteMergeRequest = getRemoteMergeRequest(repository, payload.getPullRequestNumber());
        RepositoryPullRequestMapping localPullRequest = repositoryPullRequestDao.findRequestByRemoteId(repository,
                remoteMergeRequest.getIid());

        Map<String, Participant> participantIndex = new HashMap<String,Participant>();

        try
        {
            localPullRequest = updateLocalPullRequest(repository, remoteMergeRequest, localPullRequest, participantIndex);
        }
        catch (IllegalStateException e)
        {
            // This should not happen
            LOGGER.warn("Pull request " + remoteMergeRequest.getIid() + " from repository with id " + repository.getId() + " could not be processed", e);
            // let's return prematurely
            return;
        }

        repositoryPullRequestDao.updatePullRequestIssueKeys(repository, localPullRequest.getID());

        processPullRequestComments(repository, remoteMergeRequest, localPullRequest, participantIndex);
        processPullRequestReviewComments(repository, remoteMergeRequest, localPullRequest, participantIndex);

        pullRequestService.updatePullRequestParticipants(localPullRequest.getID(), repository.getId(), participantIndex);
    }

    /**
     * Creates or updates local version of remote {@link GitlabMergeRequest}.
     * 
     * @param repository
     *            pull request owner
     * @param remoteMergeRequest
     *            remote pull request representation
     * @param localPullRequest
     * @return created/updated local pull request
     */
    private RepositoryPullRequestMapping updateLocalPullRequest(Repository repository, GitlabMergeRequest remoteMergeRequest,
            RepositoryPullRequestMapping localPullRequest, Map<String, Participant> participantIndex)
    {
        boolean shouldUpdateCommits = false;
        if (localPullRequest == null)
        {
            shouldUpdateCommits = true;
            localPullRequest = pullRequestService.createPullRequest(toDaoModelPullRequest(repository, remoteMergeRequest, null));
        }
        else
        {
            shouldUpdateCommits = shouldCommitsBeLoaded(repository, remoteMergeRequest, localPullRequest);
            localPullRequest = pullRequestService.updatePullRequest(localPullRequest.getID(), toDaoModelPullRequest(repository, remoteMergeRequest, localPullRequest));
        }

        addParticipant(participantIndex, remoteMergeRequest.getAuthor(), Participant.ROLE_PARTICIPANT);
        addParticipant(participantIndex, remoteMergeRequest.getAssignee(), Participant.ROLE_REVIEWER);

        if (shouldUpdateCommits)
        {
            updateLocalPullRequestCommits(repository, remoteMergeRequest, localPullRequest);
        }

        return localPullRequest;
    }

    private boolean shouldCommitsBeLoaded(Repository repo, GitlabMergeRequest remote, RepositoryPullRequestMapping local)
    {
        return hasStatusChanged(remote, local) || hasSourceChanged(repo, remote, local) || hasDestinationChanged(remote, local);
    }

    private boolean hasStatusChanged(GitlabMergeRequest remote, RepositoryPullRequestMapping local)
    {
        return !resolveStatus(remote).name().equals(local.getLastStatus());
    }

    private boolean hasSourceChanged(Repository repo, GitlabMergeRequest remote, RepositoryPullRequestMapping local)
    {
        return !Objects.equal(local.getSourceBranch(), getBranchName(remote.getSourceBranch(), local.getSourceBranch()))
                || !Objects.equal(local.getSourceRepo(), getRepositoryFullName(repo));
    }
    private boolean hasDestinationChanged(GitlabMergeRequest remote, RepositoryPullRequestMapping local)
    {
        return !Objects.equal(local.getDestinationBranch(), getBranchName(remote.getTargetBranch(), local.getDestinationBranch()));
    }

    private String checkNotNull(String branch, String object)
    {
        if (branch == null)
        {
            throw new IllegalStateException(object + " must not be null");
        }

        return branch;
    }

    private String getBranchName(String ref, String oldBranchName)
    {
        if (ref == null)
        {
            return oldBranchName;
        }

        return ref;
    }

    private void addParticipant(Map<String, Participant> participantIndex, GitlabUser user, String role)
    {
        if (user != null)
        {
            Participant participant = participantIndex.get(user.getUsername());

            if (participant == null)
            {
                participantIndex.put(user.getUsername(), new Participant(user.getUsername(), false, role));
            }
        }
    }

    private void updateLocalPullRequestCommits(Repository repository, GitlabMergeRequest remoteMergeRequest,
            RepositoryPullRequestMapping localPullRequest)
    {
        List<GitlabCommit> remoteCommits = getRemotePullRequestCommits(repository, remoteMergeRequest);

        Set<RepositoryCommitMapping> remainingCommitsToDelete = new HashSet<RepositoryCommitMapping>(Arrays.asList(localPullRequest
                .getCommits()));

        final Map<String, RepositoryCommitMapping> commitsIndex = Maps.uniqueIndex(remainingCommitsToDelete, new Function<RepositoryCommitMapping, String>()
        {
            @Override
            public String apply(@Nullable final RepositoryCommitMapping repositoryCommitMapping)
            {
                return repositoryCommitMapping.getNode();
            }
        });

        for (GitlabCommit remoteCommit : remoteCommits)
        {
            RepositoryCommitMapping commit = commitsIndex.get(getSha(remoteCommit));
            if (commit == null)
            {
                Map<String, Object> commitData = new HashMap<String, Object>();
                map(commitData, remoteCommit);
                commit = repositoryPullRequestDao.saveCommit(repository, commitData);
                repositoryPullRequestDao.linkCommit(repository, localPullRequest, commit);
            }
            else
            {
                remainingCommitsToDelete.remove(commit);
            }
        }

        if (!CollectionUtils.isEmpty(remainingCommitsToDelete))
        {
            LOGGER.debug("Removing commit in pull request {}", localPullRequest.getID());
            repositoryPullRequestDao.unlinkCommits(repository, localPullRequest, remainingCommitsToDelete);
            repositoryPullRequestDao.removeCommits(remainingCommitsToDelete);
        }
    }

    /**
     * Loads remote {@link org.gitlab.api.models.GitlabMergeRequest}.
     * 
     * @param repository
     *            owner of pull request
     * @param number
     *            number of pull request
     * @return remote pull request
     */
    private GitlabMergeRequest getRemoteMergeRequest(Repository repository, int number)
    {
        try
        {
            GitlabAPI gitlabAPI = gitlabClientProvider.createClient(repository);
            GitlabProject project = gitlabAPI.getProject(repository.getSlug());
            return gitlabAPI.getMergeRequest(project, number);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

    }

    /**
     * Loads remote commits for provided pull request.
     * 
     * @param repository
     *            pull request owner
     * @param remotePullRequest
     *            remote pull request
     * @return remote commits of pull request
     */
    private List<GitlabCommit> getRemotePullRequestCommits(Repository repository, GitlabMergeRequest remotePullRequest)
    {
        try {
            GitlabAPI gitlabAPI = gitlabClientProvider.createClient(repository);
            return gitlabAPI.getCommits(remotePullRequest);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Processes comments of a Pull Request.
     *
     * @param repository
     * @param remotePullRequest
     * @param localPullRequest
     */
    private void processPullRequestComments(Repository repository, GitlabMergeRequest remotePullRequest,
            RepositoryPullRequestMapping localPullRequest, Map<String, Participant> participantIndex)
    {
        updateCommentsCount(repository, remotePullRequest, localPullRequest);

        GitlabAPI gitlabAPI = gitlabClientProvider.createClient(repository);
        List<GitlabNote> pullRequestComments;
        try
        {
            pullRequestComments = gitlabAPI.getAllNotes(remotePullRequest);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        for (GitlabNote comment : pullRequestComments)
        {
            addParticipant(participantIndex, comment.getAuthor(), Participant.ROLE_PARTICIPANT);
        }
    }

    /**
     * Processes review comments of a Pull Request.
     * 
     * @param repository
     * @param remotePullRequest
     * @param localPullRequest
     */
    private void processPullRequestReviewComments(Repository repository, GitlabMergeRequest remotePullRequest,
            RepositoryPullRequestMapping localPullRequest, Map<String, Participant> participantIndex)
    {
        updateCommentsCount(repository, remotePullRequest, localPullRequest);

        GitlabAPI gitlabAPI = gitlabClientProvider.createClient(repository);
        List<GitlabNote> notes;
        try
        {
            notes = gitlabAPI.getAllNotes(remotePullRequest);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        for (GitlabNote comment : notes)
        {
            addParticipant(participantIndex, comment.getAuthor(), Participant.ROLE_PARTICIPANT);
        }
    }

    private void updateCommentsCount(Repository repo, GitlabMergeRequest remotePullRequest, RepositoryPullRequestMapping localPullRequest)
    {
        try {
            GitlabAPI gitlabAPI = gitlabClientProvider.createClient(repo);
            List<GitlabNote> comments = gitlabAPI.getAllNotes(remotePullRequest);

            localPullRequest.setCommentCount(comments.size());

            // updates count
            pullRequestService.updatePullRequest(localPullRequest.getID(), localPullRequest);
        } catch (IOException e) {

        }
    }

    @VisibleForTesting
    RepositoryPullRequestMapping toDaoModelPullRequest(Repository repository, GitlabMergeRequest source, RepositoryPullRequestMapping localPullRequest)
    {
        String sourceBranch = checkNotNull(getBranchName(source.getSourceBranch(), localPullRequest != null ? localPullRequest.getSourceBranch() : null), "Source branch");
        String dstBranch = checkNotNull(getBranchName(source.getTargetBranch(), localPullRequest != null ? localPullRequest.getDestinationBranch() : null), "Destination branch");

        PullRequestStatus prStatus = resolveStatus(source);

        RepositoryPullRequestMapping target = repositoryPullRequestDao.createPullRequest();
        target.setDomainId(repository.getId());
        target.setRemoteId((long) source.getIid());
        target.setName(ActiveObjectsUtils.stripToLimit(source.getTitle(), 255));

        //https://gitlab.fuzzhq.com/android-project/forever-21/merge_requests/2
        String host = repository.getOrgHostUrl();
        if (!host.endsWith("/")) {
            host += "/";
        }
        target.setUrl(host + repository.getSlug().replace("%2F","/")+"merge_requests/"+source.getIid());
        target.setToRepositoryId(repository.getId());

        target.setAuthor(source.getAuthor() != null ? source.getAuthor().getUsername() : null);
        target.setCreatedOn(source.getCreatedAt());
        target.setUpdatedOn(source.getUpdatedAt());
        target.setSourceRepo(getRepositoryFullName(repository));
        target.setSourceBranch(sourceBranch);
        target.setDestinationBranch(dstBranch);
        target.setLastStatus(prStatus.name());

        try {
            GitlabAPI gitlabAPI = gitlabClientProvider.createClient(repository);
            List<GitlabNote> comments = gitlabAPI.getAllNotes(source);
            target.setCommentCount(comments.size());
        } catch (IOException e)
        {

        }

        if (prStatus == PullRequestStatus.OPEN)
        {
            target.setExecutedBy(target.getAuthor());
        }

        return target;
    }

    private void map(Map<String, Object> target, GitlabCommit source)
    {
        target.put(RepositoryCommitMapping.RAW_AUTHOR, source.getAuthorName());
        target.put(RepositoryCommitMapping.MESSAGE, source.getDescription());
        target.put(RepositoryCommitMapping.NODE, getSha(source));
        target.put(RepositoryCommitMapping.DATE, source.getCreatedAt());
        target.put(RepositoryCommitMapping.MERGE, source.getParentIds() != null && source.getParentIds().size() > 1);
    }

    private String getSha(final GitlabCommit source)
    {
        return source.getId();
    }

    private PullRequestStatus resolveStatus(GitlabMergeRequest pullRequest)
    {

        if (pullRequest.isMerged()) {
            return PullRequestStatus.MERGED;
        }

        if (pullRequest.isClosed()) {
            return PullRequestStatus.DECLINED;
        }

        return PullRequestStatus.OPEN;
    }

    private String getRepositoryFullName(Repository repo)
    {
        return repo.getSlug();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MessageAddress<GitlabPullRequestSynchronizeMessage> getAddress()
    {
        return messagingService.get(GitlabPullRequestSynchronizeMessage.class, ADDRESS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getParallelThreads()
    {
        // Only one thread - comments processing is currently not thread safe!!!
        // The same comments can be proceed over the same Pull Request - because of multiple messages over the same PR
        return 1;
    }
}
