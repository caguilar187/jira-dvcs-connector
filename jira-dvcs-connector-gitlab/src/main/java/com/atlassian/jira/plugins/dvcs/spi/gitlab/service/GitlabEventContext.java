package com.atlassian.jira.plugins.dvcs.spi.gitlab.service;

import org.gitlab.api.models.GitlabMergeRequest;

/**
 * Interface defining context for GitHub event synchronisation
 */
public interface GitlabEventContext
{
    /**
     * Saving pull request
     *
     * @param pullRequest
     */
    void savePullRequest(GitlabMergeRequest pullRequest);
}
