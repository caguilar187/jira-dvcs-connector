package com.atlassian.jira.plugins.dvcs.sync;

import com.atlassian.jira.plugins.dvcs.model.Message;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.message.MessageAddress;
import com.atlassian.jira.plugins.dvcs.service.message.MessageConsumer;
import com.atlassian.jira.plugins.dvcs.service.message.MessagingService;
import com.atlassian.jira.plugins.dvcs.spi.gitlab.GitlabClientProvider;
import com.atlassian.jira.plugins.dvcs.spi.gitlab.message.GitlabPullRequestPageMessage;
import com.atlassian.jira.plugins.dvcs.spi.gitlab.service.GitlabEventService;
import com.google.common.collect.Iterables;
import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabMergeRequest;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Message consumer for {@link com.atlassian.jira.plugins.dvcs.spi.gitlab.message.GitlabPullRequestPageMessage}
 *
 * @author Miroslav Stencel <mstencel@atlassian.com>
 */
@Component
public class GitlabPullRequestPageMessageConsumer implements MessageConsumer<GitlabPullRequestPageMessage>
{
    public static final String QUEUE = GitlabPullRequestPageMessageConsumer.class.getCanonicalName();
    public static final String ADDRESS = GitlabPullRequestPageMessageConsumer.class.getCanonicalName();

    /**
     * Injected {@link com.atlassian.jira.plugins.dvcs.service.message.MessagingService} dependency.
     */
    @Resource
    private MessagingService messagingService;

    /**
     * Injected {@link com.atlassian.jira.plugins.dvcs.spi.gitlab.GitlabClientProvider} dependency.
     */
    @Resource (name = "gitlabClientProvider")
    private GitlabClientProvider gitlabClientProvider;

    /**
     * Injected {@link com.atlassian.jira.plugins.dvcs.sync.GitlabPullRequestProcessor} dependency.
     */
    @Resource
    private GitlabPullRequestProcessor gitlabPullRequestProcessor;

    /**
     * Injected {@link GitlabEventService} dependency.
     */
    @Resource
    private GitlabEventService gitlabEventService;

    @Override
    public void onReceive(final Message<GitlabPullRequestPageMessage> message, final GitlabPullRequestPageMessage payload)
    {
        Repository repository = payload.getRepository();
        GitlabAPI api = gitlabClientProvider.createClient(repository);
        String slug = repository.getSlug();
        try {
            List<GitlabMergeRequest> pullRequests = api.getAllMergeRequests(slug);
            for (GitlabMergeRequest pullRequest : pullRequests)
            {
                gitlabPullRequestProcessor.processPullRequestIfNeeded(repository, pullRequest);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fireNextPage(Message<GitlabPullRequestPageMessage> message, GitlabPullRequestPageMessage payload, int nextPage, Set<Long> proccessedPullRequests)
    {
        GitlabPullRequestPageMessage nextMessage = new GitlabPullRequestPageMessage(
                payload.getProgress(),
                payload.getSyncAuditId(),
                payload.isSoftSync(),
                payload.getRepository(),
                nextPage,
                payload.getPagelen(),
                proccessedPullRequests,
                payload.isWebHookSync());
        messagingService.publish(getAddress(), nextMessage, message.getTags());
    }

    @Override
    public String getQueue()
    {
        return QUEUE;
    }

    @Override
    public MessageAddress<GitlabPullRequestPageMessage> getAddress()
    {
        return messagingService.get(GitlabPullRequestPageMessage.class, ADDRESS);
    }

    @Override
    public int getParallelThreads()
    {
        return MessageConsumer.THREADS_PER_CONSUMER;
    }
}
