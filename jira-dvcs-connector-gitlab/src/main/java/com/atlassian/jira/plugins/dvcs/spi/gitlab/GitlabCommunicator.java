package com.atlassian.jira.plugins.dvcs.spi.gitlab;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ProtocolException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.jira.plugins.dvcs.model.ChangesetFileDetail;
import com.atlassian.jira.plugins.dvcs.model.ChangesetFileDetailsEnvelope;
import com.atlassian.jira.plugins.dvcs.service.remote.SyncDisabledHelper;
import com.atlassian.jira.plugins.dvcs.spi.gitlab.parsers.GitlabChangesetFactory;
import com.atlassian.jira.plugins.dvcs.spi.gitlab.service.GitlabEventService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.lang.StringUtils;

import org.gitlab.api.models.GitlabProject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.atlassian.jira.plugins.dvcs.auth.Authentication;
import com.atlassian.jira.plugins.dvcs.auth.OAuthStore;
import com.atlassian.jira.plugins.dvcs.auth.impl.OAuthAuthentication;
import com.atlassian.jira.plugins.dvcs.exception.SourceControlException;
import com.atlassian.jira.plugins.dvcs.model.Branch;
import com.atlassian.jira.plugins.dvcs.model.AccountInfo;
import com.atlassian.jira.plugins.dvcs.model.BranchHead;
import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.plugins.dvcs.model.DvcsUser;
import com.atlassian.jira.plugins.dvcs.model.Group;
import com.atlassian.jira.plugins.dvcs.model.Organization;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.BranchService;
import com.atlassian.jira.plugins.dvcs.service.message.MessageAddress;
import com.atlassian.jira.plugins.dvcs.service.message.MessagingService;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicator;
import com.atlassian.jira.plugins.dvcs.sync.SynchronizationFlag;
import com.atlassian.jira.plugins.dvcs.spi.gitlab.message.SynchronizeChangesetMessage;
import com.atlassian.jira.plugins.dvcs.sync.GitlabSynchronizeChangesetMessageConsumer;

import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabUser;
import org.gitlab.api.models.GitlabCommit;
import org.gitlab.api.models.GitlabCommitDiff;
import org.gitlab.api.models.GitlabBranch;
import org.gitlab.api.models.GitlabProjectHook;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class GitlabCommunicator implements DvcsCommunicator
{
    private static final Logger log = LoggerFactory.getLogger(com.atlassian.jira.plugins.dvcs.spi.gitlab.GitlabCommunicator.class);

    public static final String GITLAB = "gitlab";

    @Resource
    private  MessagingService messagingService;

    @Resource
    private  BranchService branchService;

    @Resource
    @ComponentImport
    private ApplicationProperties applicationProperties;

    /**
     * Injected {@link com.atlassian.jira.plugins.dvcs.spi.gitlab.service.GitlabEventService} dependency.
     */
    @Resource
    private GitlabEventService gitlabEventService;

    @Resource
    protected SyncDisabledHelper syncDisabledHelper;

    protected final GitlabClientProvider gitlabClientProvider;

    private final HttpClient3ProxyConfig proxyConfig = new HttpClient3ProxyConfig();
    protected final OAuthStore oAuthStore;

    @Autowired
    public GitlabCommunicator(OAuthStore oAuthStore,
                              @Qualifier("gitlabClientProvider") GitlabClientProvider gitlabClientProvider)
    {
        this.oAuthStore = oAuthStore;
        this.gitlabClientProvider = gitlabClientProvider;
    }

    @Override
    public String getDvcsType()
    {
        return GITLAB;
    }

    @Override
    public AccountInfo getAccountInfo(String hostUrl, String accountName)
    {
        GitlabAPI gitlabAPI = gitlabClientProvider.createClient(hostUrl, "");
        try
        {
            List<GitlabUser> users = gitlabAPI.findUsers(accountName);
            Iterator iterator = users.iterator();

            while (iterator.hasNext())
            {
                GitlabUser user = (GitlabUser)iterator.next();
                if (user.getUsername().equals(accountName))
                {
                    return new AccountInfo(com.atlassian.jira.plugins.dvcs.spi.gitlab.GitlabCommunicator.GITLAB);
                }
            }
        }
        catch (IOException e)
        {
            log.debug("Unable to retrieve account information. hostUrl: {}, account: {} " + e.getMessage(), hostUrl,
                    accountName);
        }
        return null;
    }

    @Override
    public List<Repository> getRepositories(Organization organization, List<Repository> storedRepositories) {
        GitlabAPI client = gitlabClientProvider.createClient(organization);
        List<org.gitlab.api.models.GitlabProject> projects = null;

        try
        {
            projects = client.getProjects();
        }
        catch (IOException e)
        {
            throw new SourceControlException("Error retrieving list of repositories", e);
        }

        // for normal account
        Iterator<org.gitlab.api.models.GitlabProject> iterator = projects.iterator();

        Set<Repository> repositories = new HashSet<Repository>();
        while (iterator.hasNext())
        {
            org.gitlab.api.models.GitlabProject project = iterator.next();
            Repository repository = new Repository();
            repository.setSlug(project.getPathWithNamespace());
            //repository.setSlug(project.getId().toString());
            repository.setName(project.getName());
            //repository.setRepositoryUrl(project.getPathWithNamespace());
            repositories.add(repository);
        }

        log.debug("Found repositories: " + repositories.size());
        return new ArrayList<Repository>(repositories);
    }

    @Override
    public Changeset getChangeset(Repository repository, String node)
    {
        GitlabAPI gitlabAPI = gitlabClientProvider.createClient(repository);
        try
        {
            GitlabCommit commit = gitlabAPI.getCommit(repository.getSlug(), node);
            List<GitlabCommitDiff> diffs = gitlabAPI.getCommitDiffs(repository.getSlug(), node);

            Changeset changeset = GitlabChangesetFactory.transform(commit, diffs, repository.getId(), null);
            changeset.setFileDetails(GitlabChangesetFactory.transformToFileDetails(diffs));

            return changeset;
        }
        catch (IOException e)
        {
            throw new SourceControlException("could not get result", e);
        }
    }

    @Override
    public ChangesetFileDetailsEnvelope getFileDetails(Repository repository, Changeset changeset) {
        try {
            GitlabAPI gitlabAPI = gitlabClientProvider.createClient(repository);
            GitlabCommit commit = gitlabAPI.getCommit(repository.getSlug(), changeset.getNode());
            List<GitlabCommitDiff> diffs = gitlabAPI.getCommitDiffs(repository.getSlug(), changeset.getNode());
            List<ChangesetFileDetail> fileDetailList = GitlabChangesetFactory.transformToFileDetails(diffs);
            return new ChangesetFileDetailsEnvelope(fileDetailList, fileDetailList.size());
        } catch (IOException e) {
            throw new SourceControlException("could not get result", e);
        }
    }

    @Override
    public void ensureHookPresent(Repository repository, String hookUrl) {
        GitlabAPI gitlabAPI = gitlabClientProvider.createClient(repository);

        String repositoryId = repository.getSlug();

        try
        {
            List<GitlabProjectHook> hooksForRepo = gitlabAPI.getProjectHooks(repositoryId);

            boolean foundChangesetHook = false;
            boolean foundPullRequesttHook = false;
            for (GitlabProjectHook hook : hooksForRepo) {
                String url = hook.getUrl();
                boolean isPullRequestHook = hook.isMergeRequestsEvents();
                if (!foundChangesetHook && hookUrl.equals(url) && !isPullRequestHook)
                {
                    foundChangesetHook = true;
                    continue;
                }

                if (!foundPullRequesttHook && hookUrl.equals(url) && isPullRequestHook)
                {
                    foundPullRequesttHook = true;
                    continue;
                }
                String thisHostAndRest = applicationProperties.getBaseUrl() + DvcsCommunicator.POST_HOOK_SUFFIX;
                String postCommitHookUrl = hook.getUrl();
                if (StringUtils.startsWith(postCommitHookUrl, thisHostAndRest))
                {
                    gitlabAPI.deleteProjectHook(hook);
                }
            }


            if (!foundChangesetHook)
            {
                gitlabAPI.addProjectHook(repositoryId, hookUrl, true, false, false);
            }

            if (!foundPullRequesttHook)
            {
                gitlabAPI.addProjectHook(repositoryId, hookUrl, true, true, true);
            }
        }
        catch (IOException e)
        {
            throw new SourceControlException.PostCommitHookRegistrationException(e.getMessage(), e);
        }
    }

    @Override
    public void removePostcommitHook(Repository repository, String postCommitUrl)
    {
        GitlabAPI gitlabAPI = gitlabClientProvider.createClient(repository);
        String repositoryId = repository.getSlug();

        try
        {
            final List<GitlabProjectHook> hooks = gitlabAPI.getProjectHooks(repositoryId);
            for (GitlabProjectHook hook : hooks)
            {
                if (postCommitUrl.equals(hook.getUrl()))
                {
                    try
                    {
                        gitlabAPI.deleteProjectHook(hook);
                    } catch (ProtocolException pe)
                    {

                    }
                }
            }
        } catch (IOException e)
        {
            throw new SourceControlException.PostCommitHookRegistrationException("Could not remove postcommit hook", e);
        }
    }

    @Override
    public String getCommitUrl(Repository repository, Changeset changeset)
    {
        return MessageFormat.format("{0}/commit/{1}", repository.getRepositoryUrl(), changeset.getNode());
    }

    @Override
    public String getFileCommitUrl(Repository repository, Changeset changeset, String file, int index)
    {
        return MessageFormat.format("{0}#diff-{1}", getCommitUrl(repository, changeset), index);
    }

    @Override
    public DvcsUser getUser(Repository repository, String userEmail)
    {
        try
        {
            GitlabAPI gitlabAPI = gitlabClientProvider.createClient(repository);

            List<GitlabUser> users = gitlabAPI.findUsers(userEmail);
            Iterator iterator = users.iterator();

            while (iterator.hasNext())
            {
                GitlabUser user = (GitlabUser)iterator.next();
                if (user.getEmail().equals(userEmail))
                {
                    String login = user.getEmail();
                    String username = user.getUsername();
                    String name = user.getName();
                    String displayName = StringUtils.isNotBlank(name) ? name : login;
                    //String gravatarUrl = ghUser.getAvatarUrl();
                    String hash = MD5Util.md5Hex(login);
                    String image = "http://www.gravatar.com/avatar/"+hash;

                    return new DvcsUser(login, displayName, null, image, repository.getOrgHostUrl() + "u/" + username);
                }
            }
            return null;
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public DvcsUser getTokenOwner(Organization organization)
    {
        try
        {
            GitlabAPI gitlabAPI = gitlabClientProvider.createClient(organization);
            GitlabUser user = gitlabAPI.getCurrentSession();

            String login = user.getUsername();
            String name = user.getName();
            String displayName = StringUtils.isNotBlank(name) ? name : login;
            String hash = MD5Util.md5Hex(login);
            String image = "http://www.gravatar.com/avatar/"+hash;

            // String gravatarUrl = user.getAvatarUrl();

            return new DvcsUser(login, displayName, null, image, organization.getHostUrl() + "/" + login);
        } catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    public List<Branch> getBranches(Repository repository)
    {
        List<Branch> branches = new ArrayList<Branch>();
        try
        {
            GitlabAPI gitlabAPI = gitlabClientProvider.createClient(repository);
            GitlabProject project = gitlabAPI.getProject(repository.getSlug());
            final List<GitlabBranch> gitlabBranches = gitlabAPI.getBranches(repository.getSlug());
            log.debug("Found branches: " + gitlabBranches.size());

            for (GitlabBranch gitlabBranch : gitlabBranches)
            {
                List<BranchHead> branchHeads = new ArrayList<BranchHead>();
                BranchHead branchTip = new BranchHead(gitlabBranch.getName(), gitlabBranch.getCommit().getId());
                branchHeads.add(branchTip);
                Branch branch = new Branch(gitlabBranch.getName());
                branch.setRepositoryId(repository.getId());
                branch.setHeads(branchHeads);

                if (project.getDefaultBranch().equalsIgnoreCase(gitlabBranch.getName()))
                {
                    branches.add(0, branch);
                } else
                {
                    branches.add(branch);
                }
            }
        } catch (IOException e)
        {
            log.info("Can not obtain branches list from repository [ "+repository.getSlug()+" ]", e);
            // we need tip changeset of the branch
            throw new SourceControlException("Could not retrieve list of branches", e);
        }
        return branches;
    }

    @Override
    public boolean supportsInvitation(Organization organization)
    {
        return false;
    }

    @Override
    public List<Group> getGroupsForOrganization(Organization organization)
    {
        return Collections.emptyList();
    }

    @Override
    public void inviteUser(Organization organization, Collection<String> groupSlugs, String userEmail) {
        throw new UnsupportedOperationException("You can not invite users to gitlab so far, ...");
    }

    @Override
    public String getBranchUrl(final Repository repository, final Branch branch)
    {
        return MessageFormat.format("{0}/{1}/{2}/tree/{3}", repository.getOrgHostUrl(), repository.getOrgName(),
                repository.getSlug(), branch.getName());
    }

    @Override
    public String getCreatePullRequestUrl(final Repository repository, final String sourceSlug, final String sourceBranch, final String destinationSlug, final String destinationBranch, final String eventSource)
    {
        return MessageFormat.format("{0}/{1}/{2}/compare/{3}...{4}",
                repository.getOrgHostUrl(),
                repository.getOrgName(),
                repository.getSlug(),
                getRef(sourceSlug, sourceBranch),
                getRef(destinationSlug, destinationBranch)
                );
    }

    @Override
    public void startSynchronisation(final Repository repo, final EnumSet<SynchronizationFlag> flags, final int auditId)
    {
        boolean softSync = flags.contains(SynchronizationFlag.SOFT_SYNC);
        boolean changesetsSync = flags.contains(SynchronizationFlag.SYNC_CHANGESETS);
        boolean pullRequestSync = flags.contains(SynchronizationFlag.SYNC_PULL_REQUESTS);
        boolean webHookSync = flags.contains(SynchronizationFlag.WEBHOOK_SYNC);

        String[] synchronizationTags = new String[] {messagingService.getTagForSynchronization(repo), messagingService.getTagForAuditSynchronization(auditId)};
        if (changesetsSync || pullRequestSync)
        {
            Date synchronizationStartedAt = new Date();
            List<Branch> branches = getBranches(repo);
            for (Branch branch : branches)
            {
                for (BranchHead branchHead : branch.getHeads())
                {
                    SynchronizeChangesetMessage message = new SynchronizeChangesetMessage(repo, //
                            branch.getName(), branchHead.getHead(), //
                            synchronizationStartedAt, //
                            null, softSync, auditId, webHookSync);
                    MessageAddress<SynchronizeChangesetMessage> key = messagingService.get( //
                            SynchronizeChangesetMessage.class, //
                            GitlabSynchronizeChangesetMessageConsumer.ADDRESS //
                    );
                    messagingService.publish(key, message, softSync ? MessagingService.SOFTSYNC_PRIORITY: MessagingService.DEFAULT_PRIORITY, messagingService.getTagForSynchronization(repo), messagingService.getTagForAuditSynchronization(auditId));
                }
            }
            List<BranchHead> oldBranchHeads = branchService.getListOfBranchHeads(repo);
            branchService.updateBranchHeads(repo, branches, oldBranchHeads);
            branchService.updateBranches(repo, branches);
        }
        if (pullRequestSync)
        {
            gitlabEventService.synchronize(repo, softSync, synchronizationTags, webHookSync);
        }
    }

    @Override
    public boolean isSyncDisabled(Repository repo, EnumSet<SynchronizationFlag> flags)
    {
        return syncDisabledHelper.isGitlabSyncDisabled();
    }

    private String getRef(String slug, String branch)
    {
        String ref = null;
        if (slug != null)
        {
            ref = slug + ":" + branch;
        } else
        {
            ref = branch;
        }

        return ref;
    }

    public static class MD5Util {
        public static String hex(byte[] array) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i]
                        & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        }
        public static String md5Hex (String message) {
            try {
                MessageDigest md =
                        MessageDigest.getInstance("MD5");
                return hex (md.digest(message.getBytes("CP1252")));
            } catch (NoSuchAlgorithmException e) {
            } catch (UnsupportedEncodingException e) {
            }
            return null;
        }
    }

    @Override
    public void linkRepository(Repository repository, Set<String> withProjectkeys)
    {

    }

    @Override
    public void linkRepositoryIncremental(Repository repository, Set<String> withPossibleNewProjectkeys)
    {

    }
}
