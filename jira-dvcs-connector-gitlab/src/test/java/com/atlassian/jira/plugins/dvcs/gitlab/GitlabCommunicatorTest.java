package com.atlassian.jira.plugins.dvcs.gitlab;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.fest.assertions.api.Assertions.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.gitlab.api.GitlabAPI;
import org.gitlab.api.models.GitlabUser;
import org.gitlab.api.models.GitlabCommit;
import org.gitlab.api.models.GitlabBranch;
import org.gitlab.api.models.GitlabBranchCommit;
import org.gitlab.api.models.GitlabCommitDiff;

import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.atlassian.jira.plugins.dvcs.auth.OAuthStore;
import com.atlassian.jira.plugins.dvcs.model.Changeset;
import com.atlassian.jira.plugins.dvcs.model.DvcsUser;
import com.atlassian.jira.plugins.dvcs.model.Repository;
import com.atlassian.jira.plugins.dvcs.service.ChangesetCache;
import com.atlassian.jira.plugins.dvcs.service.remote.DvcsCommunicator;
import com.atlassian.sal.api.net.ResponseException;

import java.lang.reflect.Field;

/**
 * @author Christopher Luu <chrisluu@fuzzproductions.com>
 */
public class GitlabCommunicatorTest
{
    @Mock
    private Repository repositoryMock;
    @Mock
    private com.atlassian.jira.plugins.dvcs.spi.gitlab.GitlabClientProvider gitlabClientProvider;
    @Mock
    private GitlabAPI gitlabAPI;
    @Mock
    private GitlabUser gitlabUser;

    // tested object
    private DvcsCommunicator communicator;

    private ChangesetCacheImpl changesetCache;

    private class ChangesetCacheImpl implements ChangesetCache
    {

        private final List<String> cache = new ArrayList<String>();

        @Override
        public boolean isCached(int repositoryId, String changesetNode)
        {
            return cache.contains(changesetNode);
        }

        public void add(String node)
        {
            cache.add(node);
        }

        @Override
        public boolean isEmpty(int repositoryId)
        {
            return cache.isEmpty();
        }
    }

    private static void printObject(Object object)
    {
        try
        {
            System.out.println(object.getClass().getName());
            for (Field field : object.getClass().getDeclaredFields())
            {
                field.setAccessible(true);
                String name = field.getName();
                Object value = field.get(object);
                System.out.printf("    %s: %s%n", name, value);
            }
        }
        catch (Throwable e)
        {
            e.printStackTrace();
        }
    }

    @BeforeMethod
    public void initializeMocksAndGitlabCommunicator()
    {
        MockitoAnnotations.initMocks(this);

        communicator = new com.atlassian.jira.plugins.dvcs.spi.gitlab.GitlabCommunicator(changesetCache = new ChangesetCacheImpl(), mock(OAuthStore.class), gitlabClientProvider);

        when(gitlabClientProvider.createClient(repositoryMock)).thenReturn(gitlabAPI);
    }

    @Test
    public void settingUpPostcommitHook_ShouldSendPOSTRequestToGitlab() throws IOException
    {
        when(repositoryMock.getOrgName()).thenReturn("ORG");
        when(repositoryMock.getSlug())   .thenReturn("1");

        communicator.setupPostcommitHook(repositoryMock, "POST-COMMIT-URL");

        verify(gitlabAPI).createProjectHook(anyInt(), anyString(), anyBoolean(), anyBoolean(), anyBoolean());
    }

    @Test
    public void gettingUser_ShouldSendGETRequestToGitlab_AndParseJsonResult() throws Exception
    {
        List<GitlabUser> allUsers = Arrays.asList(gitlabUser);
        when(gitlabAPI.getAllUsers()).thenReturn(allUsers);

        when(gitlabUser.getUsername()).thenReturn("Test GitLab user login");
        when(gitlabUser.getName()).thenReturn("Test GitLab user name");

        DvcsUser dvcsUser = communicator.getUser(repositoryMock, "Test GitLab user login");

        assertThat(dvcsUser.getUsername()).isEqualTo("Test GitLab user login");
        assertThat(dvcsUser.getFullName()).isEqualTo("Test GitLab user name");
    }

    @Test
    public void gettingDetailChangeset_ShouldSendGETRequestToGitlab_AndParseJsonResult() throws ResponseException, IOException
    {
        when(repositoryMock.getSlug()).thenReturn("1");
        when(repositoryMock.getOrgName()).thenReturn("ORG");

        GitlabCommit repositoryCommit = mock(GitlabCommit.class);
        when(repositoryCommit.getTitle()).thenReturn("ABC-123 fix");
        when(gitlabAPI.getCommit(anyString(), anyString())).thenReturn(repositoryCommit);

        Changeset detailChangeset = communicator.getChangeset(repositoryMock, "abcde");

        verify(gitlabAPI).getCommit(anyString(), anyString());

        assertThat(detailChangeset.getMessage()).isEqualTo("ABC-123 fix");
    }

    @Test
    public void getChangesets_noBranches()
    {
        // Repository
        when(repositoryMock.getSlug())   .thenReturn("1");
        when(repositoryMock.getOrgName()).thenReturn("ORG");

        Iterator<Changeset> changesetIterator = communicator.getChangesets(repositoryMock).iterator();
        assertThat(changesetIterator.hasNext()).isFalse();
        assertThat(changesetIterator.hasNext()).isFalse();

        // this should throw an exception
        try
        {
            changesetIterator.next();
        } catch (NoSuchElementException e)
        {
            return;
        }

        fail("Exception should be thrown.");
    }

    @Test
    public void getChangesets_onlyNexts() throws IOException
    {
        // Repository
        when(repositoryMock.getSlug())   .thenReturn("1");
        when(repositoryMock.getOrgName()).thenReturn("ORG");

        when(gitlabAPI.getCommitDiffs(anyString(), anyString())).thenReturn(new ArrayList<GitlabCommitDiff>());

        createSampleBranches("1");

        Iterator<Changeset> changesetIterator = communicator.getChangesets(repositoryMock).iterator();

        int changesetCounter = 0;

        while (true)
        {
            try {
                Changeset detailChangeset = changesetIterator.next();

                // we need to simulate saving of the processed changeset
                changesetCache.add(detailChangeset.getNode());

                changesetCounter++;
            } catch (NoSuchElementException e)
            {
                break;
            }
        }

        assertThat(changesetCounter).isEqualTo(5);
    }

    @Test
    public void getChangesets_twoHasNextOnLast() throws IOException
    {
        // Testing hasNext at the end of the iteration

        // Repository
        when(repositoryMock.getSlug())   .thenReturn("1");
        when(repositoryMock.getOrgName()).thenReturn("ORG");

        String repositoryId = "1";

        createBranchWithTwoNodes(repositoryId);

        Iterator<Changeset> changesetIterator = communicator.getChangesets(repositoryMock).iterator();

        changesetIterator.next();

        // we are on the last node
        assertThat(changesetIterator.hasNext()).isTrue();
        assertThat(changesetIterator.hasNext()).isTrue();
    }

    @Test
    public void getChangesets_twoHasNextOnLast2() throws IOException
    {
        // Testing hasNext at the end of the iteration, the last node is in cache

        // Repository
        when(repositoryMock.getSlug())   .thenReturn("1");
        when(repositoryMock.getOrgName()).thenReturn("ORG");

        String repositoryId = "1";

        createBranchWithTwoNodes(repositoryId);

        Iterator<Changeset> changesetIterator = communicator.getChangesets(repositoryMock).iterator();

        changesetCache.add("NODE-1");

        changesetIterator.next();
        assertThat(changesetIterator.hasNext()).isFalse();
        assertThat(changesetIterator.hasNext()).isFalse();
    }

    @Test
    public void getChangesets_twoHasNextWhenStopped() throws IOException
    {
        // Repository
        when(repositoryMock.getSlug())   .thenReturn("1");
        when(repositoryMock.getOrgName()).thenReturn("ORG");

        String repositoryId = "1";

        createBranchWithTwoNodes(repositoryId);

        Iterator<Changeset> changesetIterator = communicator.getChangesets(repositoryMock).iterator();

        changesetCache.add("MASTER-SHA");

        assertThat(changesetIterator.hasNext()).isFalse();
        assertThat(changesetIterator.hasNext()).isFalse();

        // this should throw an exception
        try
        {
            changesetIterator.next();
        } catch (NoSuchElementException e)
        {
            return;
        }

        fail("Exception should be thrown.");
    }

    @Test
    public void getChangesets_MasterBranchStopped() throws IOException
    {
        // Repository
        when(repositoryMock.getSlug())   .thenReturn("1");
        when(repositoryMock.getOrgName()).thenReturn("ORG");

        String repositoryId = "1";

        createSampleBranches(repositoryId);

        Iterator<Changeset> changesetIterator = communicator.getChangesets(repositoryMock).iterator();

        // we stop master branch
        changesetCache.add("MASTER-SHA");

        // we stopped the master branch, it should iterate branch1
        assertThat(changesetIterator.hasNext()).isTrue();
        assertThat(changesetIterator.hasNext()).isTrue();

        Changeset detailChangeset = changesetIterator.next();
        assertThat(detailChangeset.getBranch()).isEqualTo("branch1");
        assertThat(detailChangeset.getNode())  .isEqualTo("BRANCH-SHA");
    }

    @Test
    public void getChangesets_hasNext() throws IOException
    {
        // Repository
        when(repositoryMock.getSlug())   .thenReturn("1");
        when(repositoryMock.getOrgName()).thenReturn("ORG");

        String repositoryId = "1";

        createSampleBranches(repositoryId);

        Iterator<Changeset> changesetIterator = communicator.getChangesets(repositoryMock).iterator();

        int changesetCounter = 0;

        while (changesetIterator.hasNext())
        {
            changesetIterator.hasNext();
            changesetIterator.hasNext();

            Changeset detailChangeset = changesetIterator.next();
            changesetCounter++;

            // we need to simulate saving of the processed changeset
            changesetCache.add(detailChangeset.getNode());

            changesetIterator.hasNext();
            changesetIterator.hasNext();
        }

        assertThat(changesetCounter).isEqualTo(5);
    }

    @Test
    public void getChangsets_softsync() throws IOException
    {
        // Repository
        when(repositoryMock.getSlug())   .thenReturn("1");
        when(repositoryMock.getOrgName()).thenReturn("ORG");

        String repositoryId = "1";

        createSampleBranches(repositoryId);

        changesetCache.add("NODE-1");
        changesetCache.add("NODE-2");

        int changesetCounter = 0;

        for ( Changeset changeset : communicator.getChangesets(repositoryMock) )
        {
            changesetCache.add(changeset.getNode());
            changesetCounter++;
        }
        assertThat(changesetCounter).isEqualTo(3);
    }

    @Test
    public void getChangsets_fullsync() throws IOException
    {
        // Repository
        when(repositoryMock.getSlug())   .thenReturn("1");
        when(repositoryMock.getOrgName()).thenReturn("ORG");

        String repositoryId = "1";

        createMoreComplexSample(repositoryId);

        int changesetCounter = 0;

        for ( Changeset changeset : communicator.getChangesets(repositoryMock) )
        {
            changesetCache.add(changeset.getNode());
            changesetCounter++;
        }
        assertThat(changesetCounter).isEqualTo(15);
    }

    private void createBranchWithTwoNodes(String repositoryId) throws IOException
    {
     // Branches
        GitlabBranch master = createMockRepositoryBranch("MASTER", "MASTER-SHA");

     // Changeset
        mockRepositoryCommit(repositoryId, "MASTER-SHA", "ABC-123 fix",
                mockRepositoryCommit(repositoryId, "NODE-1", "ABC-123 node 1 fix"));

        when(gitlabAPI.getBranches(repositoryId)).thenReturn(Arrays.asList(master));
    }

    private void createSampleBranches(String repositoryId) throws IOException
    {
     // Branches
        GitlabBranch master = createMockRepositoryBranch("MASTER", "MASTER-SHA");
        GitlabBranch branch1 = createMockRepositoryBranch("branch1", "BRANCH-SHA");

     // Changeset
        GitlabCommit node2 = mockRepositoryCommit(repositoryId, "NODE-2", "ABC-123 node 2 fix",
                mockRepositoryCommit(repositoryId, "NODE-1", "ABC-123 node 1 fix"));

        mockRepositoryCommit(repositoryId, "MASTER-SHA", "ABC-123 node 4 fix", node2);


        mockRepositoryCommit(repositoryId, "BRANCH-SHA", "ABC-123 node 5 fix",
                mockRepositoryCommit(repositoryId, "NODE-3", "ABC-123 node 3 fix",
                    node2));

        when(gitlabAPI.getBranches(repositoryId)).thenReturn(Arrays.asList(master, branch1));
    }

    private void createMoreComplexSample(String repositoryId) throws IOException
    {
     // Branches
        GitlabBranch master = createMockRepositoryBranch("MASTER", "MASTER-SHA");
        GitlabBranch branch1 = createMockRepositoryBranch("branch1", "BRANCH-SHA");
        GitlabBranch branch2 = createMockRepositoryBranch("branch2", "BRANCH2-SHA");
        GitlabBranch branch3 = createMockRepositoryBranch("branch3", "BRANCH3-SHA");

//  B3   M  B1   B2
//               14
//          13   |
//          |    12
//       10 11  /
//      / |/| >9
//     /  8 7
//    / / |/
//  15 4  6
//   \ |  |
//     3  5
//      \ |
//        2
//        |
//        1

     // Changeset
        GitlabCommit node8;
        GitlabCommit node2;
        GitlabCommit node3;
        GitlabCommit node6;
        GitlabCommit node7;
        GitlabCommit node9;

        mockRepositoryCommit(repositoryId, "MASTER-SHA", "ABC-123 node 10 fix",
        node8 = mockRepositoryCommit(repositoryId, "NODE-8", "ABC-123 node 8 fix",
                        mockRepositoryCommit(repositoryId, "NODE-4", "ABC-123 node 4 fix",
                        node3 = mockRepositoryCommit(repositoryId, "NODE-3", "ABC-123 node 3 fix",
                                node2 = mockRepositoryCommit(repositoryId, "NODE-2", "ABC-123 node 2 fix",
                                                mockRepositoryCommit(repositoryId, "NODE-1", "ABC-123 node 1 fix")))),
                node6 = mockRepositoryCommit(repositoryId, "NODE-6", "ABC-123 node 6 fix",
                                mockRepositoryCommit(repositoryId, "NODE-5", "ABC-123 node 5 fix",
                                        node2))),
                mockRepositoryCommit(repositoryId, "BRANCH3-SHA", "ABC-123 node 15 fix",
                        node3));


        mockRepositoryCommit(repositoryId, "BRANCH-SHA", "ABC-123 node 13 fix",
                mockRepositoryCommit(repositoryId, "NODE-11", "ABC-123 node 11 fix",
                        node8,
                node7 = mockRepositoryCommit(repositoryId, "NODE-7", "ABC-123 node 7 fix",
                                node6),
                node9 = mockRepositoryCommit(repositoryId, "NODE-9", "ABC-123 node 9 fix",
                            node7)));

        mockRepositoryCommit(repositoryId, "BRANCH2-SHA", "ABC-123 node 14 fix",
                mockRepositoryCommit(repositoryId, "NODE-12", "ABC-123 node 12 fix",
                        node9));

        when(gitlabAPI.getBranches(repositoryId)).thenReturn(Arrays.asList(master, branch1, branch2, branch3));
    }

    private GitlabBranch createMockRepositoryBranch(final String name, final String topNode)
    {
        GitlabBranch repositoryBranchMock = mock(GitlabBranch.class);
        when(repositoryBranchMock.getName()).thenReturn(name);

        GitlabBranchCommit branchCommit = mock(GitlabBranchCommit.class);
        when(branchCommit.getId()).thenReturn(topNode);
        when(repositoryBranchMock.getCommit()).thenReturn(branchCommit);

        return repositoryBranchMock;
    }

    private GitlabCommit mockRepositoryCommit(final String repositoryId, final String node, final String message, GitlabCommit... parents) throws IOException
    {
        GitlabCommit repositoryCommit = mock(GitlabCommit.class);
        when(gitlabAPI.getCommit(repositoryId, node)).thenReturn(repositoryCommit);
        when(repositoryCommit.getTitle()).thenReturn(message);
        when(repositoryCommit.getId()).thenReturn(node);
        List<String> parentCommits = new ArrayList<String>();
        for (GitlabCommit parentRepositoryCommit : parents)
        {
            parentCommits.add(parentRepositoryCommit.getId());
        }
        when(repositoryCommit.getParentIds()).thenReturn(parentCommits);
        return repositoryCommit;
    }
}

