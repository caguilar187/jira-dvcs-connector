atlas-mvn -pl java-gitlab-api,jira-dvcs-connector-gitlab,jira-dvcs-connector-plugin clean
#atlas-mvn -DskipTests -pl java-gitlab-api,jira-dvcs-connector-gitlab install
#mvn -Dmaven.test.skip=true -DskipTests -pl jira-dvcs-connector-api,jira-dvcs-connector-pageobjects install
#mvn -Dmaven.test.skip=true -DskipTests -pl bitbucket-client,jira-dvcs-connector-bitbucket install
#mvn -Dmaven.test.skip=true -DskipTests -pl jira-dvcs-connector-github,jira-dvcs-connector-github-enterprise install
mvn -Dmaven.test.skip=true -DskipTests -pl jira-dvcs-connector-api,java-gitlab-api,jira-dvcs-connector-gitlab install
atlas-mvn -Dmaven.test.skip=true -DskipTests -pl jira-dvcs-connector-plugin -U com.atlassian.maven.plugins:maven-amps-dispatcher-plugin:4.2.10:debug
#cd jira-dvcs-connector-plugin && atlas-debug
#cd .
